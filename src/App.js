import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };
  handleComplete = (event, item) => {
    let stateClone = [...this.state.todos];
    const clone = stateClone.map((todo) => {
      if (todo.id === item) {
        todo.completed = !todo.completed
      }
    return todo
    })
    this.setState({ todos: clone })
  }
  addHandler = (event) => {
    if (event.key === "Enter") {
      let stateClone = [...this.state.todos];
      const newItem = {
        userId: 1,
        id: Math.ceil(Math.random() * 99999),
        title: event.target.value,
        completed: false,
      };
      stateClone.push(newItem);
      this.setState({ todos: stateClone });
    }
  };
  handleDelete = (event, itemId) => {
      let stateClone = [...this.state.todos]
      const clone = stateClone.filter((item) => item.id !== itemId)
    this.setState({ todos: clone })
  };
  handleClear = (event) => {
      let stateClone = [...this.state.todos];
      const clone = stateClone.filter((item) => item.completed !== true)
   this.setState({ todos: clone })
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            type="text"
            placeholder="What needs to be done?"
            onKeyDown={this.addHandler}
            autoFocus
          />
        </header>
        <TodoList todos={this.state.todos} handleDelete={this.handleDelete} handleComplete={this.handleComplete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleClear}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick={ (event) => this.props.handleComplete(event, this.props.id)}
          /> 
          <label>{this.props.title}</label>
          {<button 
          className="destroy"
          onClick={ (event) => this.props.handleDelete(event, this.props.id)}
          />}
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              handleDelete={this.props.handleDelete}
              handleComplete={this.props.handleComplete}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;